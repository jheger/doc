# Privacy Policy

Version 2.

Last revised on 10 August 2019.


## (1) General Information

This privacy policy applies to the add-ons for Atlassian applications which are provided by Johannes Heger
on https://marketplace.atlassian.com/vendors/1212081/johannes-heger.

If you have any security or privacy related question, please contact me at ‘gldog (at) gmx.de'.

## (2) E-Mail Contact

It is possible to contact me via the e-mail address provided. In this case, personally identifiable information (PII) with the email of the user are stored. In this context, there is no transfer of data to third parties. The data are used solely for the processing of the conversation.

Legal basis for the processing of the data transferred in the course of sending e-mail is Article 6 (1) (f) GDPR. The mail contact aimed at the conclusion of a contract, additional legal basis for the processing is Article 6 (1) (b) GDPR.The personal data processing is done only to process your contact request.The data will be deleted when it is no longer necessary for the achievement of the purpose of their collection.The user has the possibility to withdraw their consent for the processing of personal data at any time. For the revocation, a short email to the provided e-mail address with the request to delete the personal data ranges including a contradiction of the storage is sufficient.


## (3) Atlassian Marketplace
I offer software products on the Atlassian marketplace at https://marketplace.atlassian.com. To install the software and later to purchase it, you must send data to Atlassian. This data transfer is carried out in accordance with the data protection policy of Atlassian®: https://www.atlassian.com/legal/privacy-policy.

If you request an Update Request via the Atlassian Marketplace, Atlassian sends me an automated e-mail notification.
If you’re logged in to the Atlassian Marketplace doing so, your name and e-mail address is part of this notification. I will retain these e-mail-notifications at a maximum of 1 year.

## (4) Issue Tracker

If you file an issue in the add-on’s issue trackers, anyone can access the data you provide.

Be careful with abstracts, attached logfiles or screenshots, as they could expose personal data. I'm not accountable in any way for data provided by you.

I have no write-access to the author-field of the issues. Therefore, I can’t clear it.


## (5) What Personal Data The Apps Store

None of my apps store or send data outside the Atlassian host-applications they are installed in.


## (6) Privacy Rights
Each person concerned has the right for information under Article 15 of the GDPR, the right of correction under Article 16 GDPR, the right to cancellation under Article 17 GDPR, the right to restriction of processing under Article 18 GDPR, the right to object under Article 21 GDPR and the right to data portability under Article 20 GDPR.
With regard to the right of access and the right of cancellation, the restrictions under sections 34 and 35 BDSG apply. In addition, there is a right of appeal to a competent data protection supervisory authority (Article 77 GDPR in conjunction with section 19 BDSG).


## (7) Information About your Right Of Objection In Accordance With Article 21 GDPR
### (7.1) Law of contradiction in individual cases
You have the right, at any time, to object to the processing of personal data relating to you under article 6 (1) (e) or (f) of the GDPR for reasons arising from your particular situation.If you object, we will no longer process your personal information unless we can establish compelling legitimate grounds for processing that outweigh your interests, rights and freedoms, or the processing is for the purpose of enforcing, pursuing or defending legal claims.
### (7.2) Right of objection against processing of data for direct marketing purposes
If the personal data relating to you is processed in order to operate direct mail, you have the right to object at any time to the processing of your personal data for the purposes of such advertising.If you object to processing for direct marketing purposes, your personal information will no longer be processed for these purposes.
### (7.3) Receiver of an objection
The opposition can be done informally via email with the subject "Objection", stating your name and email address to ‘gldog (at) gmx.de'.


