# General Software Product EULA

END-USER LICENSE AGREEMENT FOR JOHANNES HEGER SOFTWARE PRODUCTS. IMPORTANT PLEASE READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY BEFORE CONTINUING WITH THIS PROGRAM INSTALL: Johannes Heger's End-User License Agreement ("EULA") is a legal agreement between you (either an individual or a single entity) and Johannes Heger. for Johannes Heger software product(s) identified above which may include associated software components, media, printed materials, and "online" or electronic documentation ("SOFTWARE PRODUCT"). By installing, copying, or otherwise using the SOFTWARE PRODUCT, you agree to be bound by the terms of this EULA. This license agreement represents the entire agreement concerning the program between you and Johannes Heger, (referred to as "licenser"), and it supersedes any prior proposal, representation, or understanding between the parties. If you do not agree to the terms of this EULA, do not install or use the SOFTWARE PRODUCT.

The SOFTWARE PRODUCT is protected by copyright laws and international copyright treaties, as well as other intellectual property laws and treaties. The SOFTWARE PRODUCT is licensed, not sold.


## 1. Grant of license

The SOFTWARE PRODUCT is licensed as follows:

(a) Installation and Use

The licenser grants you the non-exclusive and non-transferable right to install and use one copy of the SOFTWARE PRODUCT on one Production instance of a host Atlassian product. You may also install and use the SOFTWARE PRODUCT on any number of Developer-licensed (derived from Production license) instances of a host Atlassian product for non-production use only

(b) Backup Copies

You may also make copies of the SOFTWARE PRODUCT as may be necessary for backup and archival purposes. You must keep all copies of the SOFTWARE PRODUCT secure and maintain accurate and up-to-date records of the number and locations of all copies of the Software.


## 2. Description of other rights and limitations

(a) Maintenance of Copyright Notices

You must not remove or alter any copyright notices on any and all copies of the SOFTWARE PRODUCT.

(b) Distribution

You may not distribute registered copies of the SOFTWARE PRODUCT to third parties. Evaluation versions available for download from the licenser websites may be freely distributed.

(c) Prohibition on Reverse Engineering, Decompilation, and Disassembly.

You may not reverse engineer, decompile, or disassemble the SOFTWARE PRODUCT, except and only to the extent that such activity is expressly permitted by applicable law notwithstanding this limitation.

(d) Rental and modification

You may not rent, lease, or lend, merge, adapt, vary, alter or modify the SOFTWARE PRODUCT save as set out in clause 8 or as otherwise agreed in writing.

(e) Support Services

The licenser may provide you with support services related to the SOFTWARE PRODUCT ("Support Services"). Any supplemental software code provided to you as part of the Support Services shall be considered part of the SOFTWARE PRODUCT and subject to the terms and conditions of this EULA.

(f) Compliance with Applicable Laws

You must comply with all applicable laws regarding use of the SOFTWARE PRODUCT.


## 3. Termination

Without prejudice to any other rights, the licenser may terminate this EULA if you fail to comply with the terms and conditions of this EULA. In such event, all rights granted to you under this EULA shall cease, you must immediately cease all activities authorised by this EULA and you must destroy all copies of the SOFTWARE PRODUCT in your possession.


## 4. Copyright

All title to intellectual property rights, including but not limited to copyrights, in and to the SOFTWARE PRODUCT and any copies thereof are owned by the licenser or its suppliers. All title and intellectual property rights in and to the content which may be accessed through use of the SOFTWARE PRODUCT is the property of the respective content owner and may be protected by applicable copyright or other intellectual property laws and treaties. This EULA grants you no rights to use such content. All rights not expressly granted are reserved by the licenser.


## 5. No warranties

The licenser expressly disclaims any warranty for the SOFTWARE PRODUCT. The SOFTWARE PRODUCT is provided 'As Is' without any express or implied warranty of any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness of a particular purpose. The licenser does not warrant or assume responsibility for the accuracy or completeness of any information, text, graphics, links or other items contained within the SOFTWARE PRODUCT. The licenser makes no warranties respecting any harm that may be caused by the transmission of a computer virus, worm, time bomb, logic bomb, or other such computer program. The licenser further expressly disclaims any warranty or representation to Authorized Users or to any third party.


## 6. Limitation of liability

(a) In no event shall the licenser be liable for any damages (including, without limitation, lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or inability to use the SOFTWARE PRODUCT, even if the licenser has been advised of the possibility of such damages. In no event will the licenser be liable for loss of data direct or indirect loss of profits or anticipated savings or for indirect, special, incidental, consequential losses, or other damages based in contract, tort or otherwise. The licenser shall have no liability with respect to the content of the SOFTWARE PRODUCT or any part thereof, including but not limited to errors or omissions contained therein, libel, infringements of rights of publicity, privacy, trademark rights, business interruption, personal injury, loss of privacy, moral rights or the disclosure of confidential information.

(b) You acknowledge that the SOFTWARE PRODUCT has not been developed to meet your individual requirements, and that it is therefore your responsibility to ensure that the facilities and functions of the Software as described in the Documents meet your requirements.

(c) The licenser’s maximum aggregate liability under or in connection with this EULA whether in contract, tort (including negligence) or otherwise, shall in all circumstances (save as set out in paragraph 6(d) below be limited to a sum equal to 100% of annual the licence fee payable by you under the EULA.

(d) The licenser do not exclude or limit in any way its liability to you where it would be unlawful to do so. This includes liability for death or personal injury caused by its negligence or the negligence of its employees, agents or subcontractors or for fraud or fraudulent misrepresentation


## 7. Evaluation software

This paragraph applies to any Software that the licenser makes available on an evaluation basis ("Evaluation Software"). End User may only use the Evaluation Software for internal evaluation purposes for the period specified by the single use freely available evaluation License, and may only permit a limited number of users (specified by the licenser) to access the Evaluation Software. After the evaluation period, End User must delete all copies of the Evaluation Software. End User acknowledges that Evaluation Software may not be fully functional. Notwithstanding anything else in this Agreement, the licenser does not offer any warranty, indemnity for any Evaluation Software.


## 8. Source code

The licenser may provide through online documentation or support some elements of Software in source code form ("Source Code"). Unless otherwise specified, End User may modify Source Code solely to develop bug fixes, customizations, and additional features ("End User Modifications") and, notwithstanding anything else in this Agreement, may only use End User Modifications internally for purposes of using the Software licensed from the licenser. The licenser will have no support, warranty, indemnity or other obligations relating to, and assumes no liability for, any End User Modifications or any effect they may have on the operation of the Products.


## 9. Publicity rights

The licenser may identify you as an Atlassian customer in our promotional materials. You may request that we stop doing so by submitting an email to "gldog (at) gmx.de" at any time. Please note that it may take the licenser up to 30 days to process your request.


## 10. Data protection

You acknowledge and agree that for the purposes of European Union legislation relating to processing of personal data (Data Protection Legislation), the Company will not be required to process any personal data (as defined in the Data Protection Legislation) for or on your behalf for the purpose of this EULA.

The licenser will only use your personal information as set out in [Privacy Policy](https://bitbucket.org/jheger/doc/src/master/privacy-policy.md).


## 11. Other important terms

The licenser may transfer its rights and obligations under this EULA to another organisation, but this will not affect your rights or its obligations under this EULA.

You may only transfer your rights or your obligations under this EULA to another person if the licenser agrees in writing.

This EULA, its subject matter and its formation (and any non-contractual disputes or claims) are governed by German law. The licenser and you both irrevocably agree to the exclusive jurisdiction of the courts of Germany / Hamburg.


## 12. Change

Where you have subscribed for the SOFTWARE PRODUCT on an period basis, the licenser reserves the right to amend this EULA from time to time with such changes being notified to you and coming into effect on each renewal of the EULA.

