# Data Center Compatibility Statement

Dear Data Center users!

Atlassian sharpens the rules for the Data Center (DC) compatibility, starting in fall 2018.
From this, Atlassian gives a comprehensive checklist and compatibility process to
add-on developers.
Along with this new compatibility process, Atlassian removes the DC compatibility flags
set so far and will re-set it if an add-on passed this process.


Before this, it was up on the developer to set DC compatibility based on some
“rules of thumb” given by Atlassian, which was quite easy to fulfill.
I did basic compatibility tests on my local dev-environment according to these rules
and they passed.

Now we developers are in a much more professional situation, as we have to fulfill the
Atlassian DC process.
Of course, this is very good for DC customers.
But in my situation as a hobby developer, I can’t fulfill it.
My add-ons won’t get back the DC compatibility seal.

In my opinion, my add-ons “Tag Details And Git Notes” and “My Branches” compiled and
listed for Bitbucket 5.x Server are still compatible and functional in Bitbucket 5.x DC
instances, and they will be functional for all Bitbucket 5.x versions.
The same for my Jira add-on “TROCK”: Versions compiled for Jira 7.x are functional
for all Jira 7.x Server and Data Center versions.

In detail, the compatibilities are:

- Bitbucket Server/DC 4.0.0 - 5.9.1: “Tag Details And Git Notes” 2.4.0
- Bitbucket Server/DC 5.10.0 < 6 : “Tag Details And Git Notes” >= 3.0.0
- Bitbucket Server/DC 4.0.0 - 5.9.1: “My Branches” 3.1.0
- Bitbucket Server/DC 5.10.0 < 6: “My Branches” >= 4.0.1
- Jira 7.1 < 8: "TROCK" >= 2.5.0

I hope this doesn’t deter you from using my add-ons,

Best regards, Johannes
